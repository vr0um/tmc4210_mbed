/*
 * TMC4210 simple test sketch.
 *
 * The position, speed and acceleration as reported by the motion controller is printed on the serial port every 50ms.
 *
 * Tested on ESP32
 *
 * Tom Magnier <tom@tmagnier.fr> 07/2018
 */
#include "mbed.h"
#include "USBSerial.h"
#include "TMC4210.h"

#define TMC4210_CS_PIN      PC_9
#define TMC4210_CLK_PIN     PC_8
#define SPI_SCK_PIN         PC_10
#define SPI_MISO_PIN        PC_11
#define SPI_MOSI_PIN        PC_12

// #define TMC4210_CS_PIN      33
// #define TMC4210_CLK_PIN     32
// #define SPI_SCK_PIN         5
// #define SPI_MISO_PIN        17
// #define SPI_MOSI_PIN        16

#define TMC4210_CLK_FREQ    16000000L
#define TMC4210_CLK_PERIOD  0.0000001

TMC4210 tmc;
USBSerial serial;

/* DigitalOut tmc_cs(TMC4210_CS_PIN); */

PwmOut tmc_clk(TMC4210_CLK_PIN);

SPI spi(SPI_MOSI_PIN, SPI_MISO_PIN, SPI_SCK_PIN);

int main(void) {
    serial.printf("Starting TMC4210 simple test...\r\n");

    /* Init TMC4210 clock */
    serial.printf("Starting Clock ...\r\n");
    tmc_clk.period_us(1.0f);
    serial.printf("Starting setting pwm ...\r\n");
    tmc_clk.write(0.50f);

    /* pinMode(TMC4210_CLK_PIN, OUTPUT); */
    /* ledcSetup(0, TMC4210_CLK_FREQ, 2); //2 bits */
    /* ledcAttachPin(TMC4210_CLK_PIN, 0); */
    /* ledcWrite(0, 2); */


    // SPI init
    serial.printf("Starting TMC ...\r\n");
    /* spi.frequency(1000000); */
    /* SPI.begin(SPI_SCK_PIN, SPI_MISO_PIN, SPI_MOSI_PIN); */
    tmc.begin(&spi, TMC4210_CLK_FREQ, TMC4210_CS_PIN, 200*16*2, 200*16*8);
    serial.printf("Target : 1000...\r\n");
    tmc.setTargetPosition(200);
    tmc.setAcceleration(400);
    tmc.setTargetSpeed(200);
    serial.printf("Target is : %li \r\n", tmc.getTargetPosition());

    while(1) {
        serial.printf("Pos\t%li", tmc.getCurrentPosition());
        serial.printf("\tSpeed\t%i", tmc.getCurrentSpeed());
        serial.printf("\tAccel\t%i", tmc.getCurrentAcceleration());
        serial.printf("\r\n");

        if (tmc.isTargetReached())
        {
            long nextTarget = tmc.getCurrentPosition() < 0 ? 200 : -200;
            tmc.setTargetPosition(nextTarget);
            serial.printf("New target is : %li \r\n", tmc.getTargetPosition());
            serial.printf("\r\n");
        }

        ThisThread::sleep_for(50);
    }
}
